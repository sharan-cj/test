import React,{useState} from 'react';
import './App.css';
import axios from 'axios';

function App() {

  const server = `http://172.104.59.184/Order Processing/order`;

  const [data, setData] = useState(null);
  const [address, setAddress] = useState(65);
  const [amount, setAmount] = useState(150);
  const [user, setUser]= useState(88);
  

  const fileHandler = event =>{
    const file = event.target.files[0];
    setData(file);
    
  }


  const uploadHandler = event => {
    event.preventDefault();
    const formData = new FormData();
    formData.append('prescription_files',data);
    submitData(formData);
  }

  async function submitData(formData){
    await axios.post(server,{
      address_id: address,
      order_amount : amount,
      order_status_code: "INIT",
      user_id: user,
      promoCode: 'FLAT20',
      formData
    })
    .then(res => {
      console.log(res);
    })
  }

  return (
    <div className="App">
      <div>Prescription:
        <input type='file' accept='image/*' onChange={fileHandler}/>
      </div>
      <div>User ID:
        <input type='number' placeholder='leave blank for default value 88' onChange={(event)=>{setUser(event.target.value)}} />
      </div>
      <div>Order Amount:
        <input type='number' placeholder='default 150' onChange={e=>{setAmount(e.target.value)}} />
      </div>
      <div>Address ID:
        <input type='number' placeholder='default 65' onChange={e=>{setAddress(e.target.value)}} />
      </div>
     <button onClick={uploadHandler}>Upload</button>

    </div>
  );
}

export default App;
